Use Remindr
===========
After the configuration of Remindr, just launch the following command::

    $ remindr -c /path/to/remindr.ini

Run Remindr on a regular basis
==============================
Remindr should be launched on a regular basis in order to efficiently remind people about your blog posts using the social networks. It is quite easy to achieve with adding a line to your user crontab, as described below::

    0 11 * * * johndoe remindr -c /path/to/remindr.ini

will execute remindr every day at 11AM.

Test option
===========
In order to know what's going to be sent to Mastodon without actually doing it, use the **--dry-run** option::

    $ remindr --dry-run -c /path/to/remindr.ini

Debug option
============
In order to increase the verbosity of what's Remindr is doing, use the **--debug** option followed by the level of verbosity see [the the available different levels](https://docs.python.org/3/library/logging.html)::

    $ remindr --debug -c /path/to/remindr.ini

Using syslog
============
Remindr is able to send its log to syslog. You can use it with the following command::

    $ remindr --syslog=WARN -c /path/to/remindr.ini
